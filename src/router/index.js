import Vue from 'vue'
import Router from 'vue-router'
import NewGame from '@/components/NewGame'
import GameLayout from '@/components/GameLayout'
import GameOver from '@/components/GameOver'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'root',
      component: NewGame
    },
    {
      path: '/game',
      name: 'game',
      component: GameLayout
    },
    {
      path: '/gameover',
      name: 'gameover',
      component: GameOver
    }
  ]
})

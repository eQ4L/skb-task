import { sampleSize, cloneDeep, shuffle } from 'lodash'

/**
 * Массив всех карт
 * @param {Object} state
 * @param {Array} cards
 */
export const setDeck = (state, cards) => {
  state.deck = cards
}

/**
 * Массив выбранных карт, участвующих в данной раздаче (игре)
 * @param {Object} state
 * @param {Array} cards
 */
export const setRandomCards = (state) => {
  let cards = sampleSize(state.deck, 9).map(card => {
    return {
      isFlipped: true,
      isFound: false,
      value: card.replace('.png', '')
    }
  })
  cards = [].concat(cloneDeep(cards), cloneDeep(cards))
  state.cards = shuffle(cards)
}

/**
 * Переворачивает карту (рубашкой вниз), по которой кликнули
 * @param {Object} state
 * @param {Number} key
 */
export const flipCard = (state, key) => {
  state.cards[key].isFlipped = !state.cards[key].isFlipped
}

/**
 * Переворачивает все карты рубашкой вверх
 * @param {Object} state
 * @returns {Array} state.cards - массив перевернутых карт
 */
export const invertCards = (state) => {
  state.cards = state.cards.map(card => ({
    value: card.value,
    isFlipped: false,
    isFound: card.isFound
  }))
}

/**
 * Помечает найденные карты
 * @param {Object} state
 * @returns {Array} state.cards - массив карт
 */
export const markFoundCards = (state) => {
  state.cards = state.cards.map(card => {
    if (card.isFlipped) {
      card.isFound = true
    }

    return card
  })
}

/**
 * Задает найденные карты
 * @param {Object} state
 * @returns {Array} - Массив найденных карт
 */
export const setFoundCards = (state) => {
  state.foundCards = state.cards.filter(card => {
    return card.isFound
  })
}

/**
 * Прибавляет очки
 * @param {Object} state
 */
export const addsScore = (state) => {
  const concealedCards = state.cards.filter(card => {
    return !card.isFound
  })
  const coeff = (concealedCards.length / 2)
  state.score += (coeff * 42)
}

/**
 * Отнимает очки
 * @param {Object} state
 */
export const subtractsScore = (state) => {
  const foundCards = state.foundCards
  if (foundCards === null) return
  const coeff = (foundCards.length / 2)

  state.score -= (coeff * 42)
  if (state.score < 0) {
    state.score = 0
  }
}

/**
 * Сбрасывает счетчик очков
 * @param {Object} state
 */
export const clearScore = (state) => {
  state.score = 0
}

/**
 * Очищает массив найденных пар
 * @param {Object} state
 */
export const clearFoundCards = (state) => {
  state.foundCards = null
}

/**
 * Статус игры - "Новая игра"
 * @param {Object} state
 */
export const newGame = (state) => {
  state.isGameOver = false
}

/**
 * Статус игры - "игра закончена"
 * @param {Object} state
 */
export const gameOver = (state) => {
  state.isGameOver = true
}

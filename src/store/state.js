export default {
  deck: null,
  cards: null,
  foundCards: null,
  isGameOver: false,
  score: null
}

export const setDeck = ({commit}, cards) => {
  commit('setDeck', cards)
}

export const setRandomCards = ({commit}) => {
  commit('setRandomCards')
}

export const flipCard = ({commit}, key) => {
  commit('flipCard', key)
}

export const invertCards = ({commit}) => {
  commit('invertCards')
}

export const markFoundCards = ({commit}) => {
  commit('markFoundCards')
}

export const setFoundCards = ({commit}) => {
  commit('setFoundCards')
}

export const clearScore = ({commit}) => {
  commit('clearScore')
}

export const clearFoundCards = ({commit}) => {
  commit('clearFoundCards')
}

export const addsScore = ({commit}) => {
  commit('addsScore')
}

export const subtractsScore = ({commit}) => {
  commit('subtractsScore')
}

export const newGame = ({commit}) => {
  commit('newGame')
}

export const gameOver = ({commit}) => {
  commit('gameOver')
}

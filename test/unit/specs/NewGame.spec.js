import Vue from 'vue'
import router from '@/router'
import NewGame from '@/components/NewGame'

describe('NewGame.vue', () => {
  const Constructor = Vue.extend(NewGame)
  const vm = new Constructor({router}).$mount()
  it('Правильное содержание на страничке', () => {
    expect(vm.$el.querySelector('.app__name').textContent)
      .toEqual('Memory Game');
  })

  it('Ожидаемая html структура', () => {
    expect(vm.$el).toMatchSnapshot()
  })
})

import Vue from 'vue'
import router from '@/router'
import GameLayout from '@/components/GameLayout'

describe('GameLayout.vue', () => {
  const Constructor = Vue.extend(GameLayout)
  const vm = new Constructor({router}).$mount()

  it('Ожидаемая html структура', () => {
    expect(vm.$el).toMatchSnapshot()
  })
})

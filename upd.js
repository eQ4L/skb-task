const path = require('path');
const fs = require('fs');

const input = __dirname + '/src/assets/cards';
const output = __dirname + '/src/helpers/cards.json';

new Promise((resolve, reject) => {
    fs.readdir(input , (err, data) => {
        console.log('Data fetched')
        resolve(JSON.stringify(data))
    })
})
    .then(data => {
        fs.writeFileSync(output, data)
        console.log('File updated');
    })
    .catch(e => console.log('Error', e))